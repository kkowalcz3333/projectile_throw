%macro draw_5 0
	mov [rsi + 8], rax
	mov [rsi + 16], rax 
	mov [rsi + 24], eax 
	add rsi, 4096
%endmacro

%macro draw_7 0
	mov [rsi + 4], rax
	mov [rsi + 12], rax
	mov [rsi + 20], rax
	mov [rsi + 28], eax 
	add rsi, 4096
%endmacro

%macro draw_9 0
	mov [rsi], rax
	mov [rsi + 8], rax
	mov [rsi + 16], rax 
	mov [rsi + 24], rax 
	mov [rsi + 32], eax
	add rsi, 4096
%endmacro

section .data
align 16
gravity: dq -4.903325, -9.80665
minus: dq 0x8000000000000000, 0x8000000000000000
div_two: dq 0.5, 0.5
nine: dq 9.0, 9.0

; rdi: allegro bitmap (ptr)
; rsi: velocity [v_y, v_x] (ptr)
; rdx: position [pos_y, pos_x] (ptr)
; xmm0: K - friction multiplier
; xmm1: time in seconds to calculate

section .text
global throw
throw:	
	; delta_v_x = K * v^2 / m / t, assuming m=1
	; delta_v_y = K * ((v+(v-9.81t))/2)^2 * t / m - 9.81t
	;			  = K * (-4.9t + v)^2 * t / m - 9.81t; assuming m=1
	movapd xmm7, [rsi]			; for delta_v and pos
	movapd xmm6, xmm7			; copy for return
	movapd xmm4, xmm6			; copy for distance measuring
	punpcklqdq xmm0, xmm0
	punpcklqdq xmm1, xmm1		; copy K and t to upper part of their registers
	
	movapd xmm5, [gravity]		; xmm5: gravity * t
	mulpd xmm5, xmm1
	addsd xmm7, xmm5			; -4.9t for v_y
	punpckhqdq xmm5, xmm5		; bring -9.81t to lower part of xmm5
	
	mulpd xmm7, xmm7
	mulpd xmm7, xmm0
	mulpd xmm7, xmm1			; ^2 * K * t for both
	por xmm7, [minus]			; make delta_v values negative
	addsd xmm7, xmm5			; - 9.81t for v_y
	addpd xmm6, xmm7
	movapd [rsi], xmm6			; return new speeds

	; delta_s = v0 * t + delta_v * t / 2
	;		  = t * (delta_v/2 + v0)
	mulpd xmm7, [div_two]
	addpd xmm7, xmm4
	mulpd xmm7, xmm1
	addpd xmm7, [rdx]
	movapd [rdx], xmm7

	mulpd xmm7, [nine]			; scale the positions on screen so that the ball (9px) has 1 m diameter
	cvtsd2si r10, xmm7
	punpckhqdq xmm7, xmm7
	cvtsd2si r9, xmm7			; r9: x, r10: y

	; ALLEGRO BITMAP PROCESSING

	cmp r9, 0
	jl end
	cmp r10, 0
	jl end
	cmp r10, 504
	jge end
	cmp r9, 1016
	jge end						; dont draw balls that dont fully fit on the screen
					
	sub rdi, 2080752			; pixels in allegro bitmap are stored backwards, skip first 4 rows and columns
	mov rsi, rdi				; rsi - location of center pixel
	shl r10, 12	
	add rsi, r10
	shl r9, 2
	add rsi, r9
	sub rsi, 16400				; move to bottom-left corner
	mov rax, 0x00FFFF0000FFFF00

	draw_5
	draw_7
	draw_9
	draw_9
	draw_9
	draw_9
	draw_9
	draw_7
	draw_5

end: ret