CC = gcc
FLAGS = -g -Wall -no-pie

main: main.o throw.o
	$(CC) $(FLAGS) main.o throw.o -o main $$(pkg-config allegro-5 allegro_font-5 --libs --cflags) -lm

main.o: main.c
	$(CC) $(FLAGS) -c main.c 

throw.o: throw.s
	nasm -g -f elf64 throw.s

clean:
	rm -rf *.o
	rm -rf main