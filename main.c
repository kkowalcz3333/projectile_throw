#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/bitmap.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "throw.h"

#define charadd() if (strlen(inputs[selected_line]) != 19) strcat(inputs[selected_line], &ch);

int main(void) {
	double k = 0;
	// coordinate vectors are [y, x]
	double* v = (double*)malloc(2 * sizeof(double));
	double* pos = (double*)malloc(2 * sizeof(double));
	pos[0] = 0;
	pos[1] = 0;
	double time = 1.0 / 60.0;
	const int X_LIM = 1024 / 9;

	al_init();
	al_install_keyboard();
	ALLEGRO_TIMER* timer = al_create_timer(time);
	ALLEGRO_EVENT_QUEUE* queue = al_create_event_queue();
	ALLEGRO_DISPLAY* disp = al_create_display(1024, 512);
	ALLEGRO_FONT* font = al_create_builtin_font();
	ALLEGRO_BITMAP* display_bitmap = al_get_backbuffer(disp);
	al_set_target_bitmap(display_bitmap);
	al_set_target_backbuffer(disp);
	al_register_event_source(queue, al_get_keyboard_event_source());
	al_register_event_source(queue, al_get_display_event_source(disp));
	al_register_event_source(queue, al_get_timer_event_source(timer));

	bool redraw = true;
	bool quit = false;
	bool pressed_key = false;
	bool launching = false;
	int selected_line = 0;
	char ch = 0;
	char inputs[3][20] = { "0.01", "30", "50" };
	bool dot_used[3] = { true, false, false };
	ALLEGRO_EVENT event;

	al_start_timer(timer);
	while (!quit)
	{
		al_wait_for_event(queue, &event);

		if (event.type == ALLEGRO_EVENT_TIMER) redraw = true;
		else if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE) quit = true;
		else if (event.type == ALLEGRO_EVENT_KEY_DOWN && !pressed_key) {
			pressed_key = true;
			switch (event.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				if (selected_line != 0) selected_line--;
				break;
			case ALLEGRO_KEY_DOWN:
				if (selected_line != 2) selected_line++;
				break;
			case ALLEGRO_KEY_ENTER:
				if (!launching) {
					launching = true;

					double angle = strtod(inputs[1], NULL);
					if (angle > 90) launching = false;
					angle = angle / 180 * M_PI;

					v[0] = strtod(inputs[2], NULL);
					v[1] = cos(angle) * v[0];
					v[0] *= sin(angle);
					pos[1] = 0;
					pos[0] = 0;
					k = strtod(inputs[0], NULL);	
					if (k > 1) launching = false;
				}
				break;
			case ALLEGRO_KEY_ESCAPE:
				quit = true;
				break;
			case ALLEGRO_KEY_BACKSPACE:
				char* c = &inputs[selected_line][strlen(inputs[selected_line]) - 1];
				if (*c == ',' || *c == '.') dot_used[selected_line] = false;
				*c = '\0';
				break;
			case ALLEGRO_KEY_COMMA:
			case ALLEGRO_KEY_FULLSTOP:
				if (!dot_used[selected_line] && strlen(inputs[selected_line]) != 19) {
					char c = '.';
					strcat(inputs[selected_line], &c);
					dot_used[selected_line] = true;
				}
				break;
			case ALLEGRO_KEY_0:
				ch = '0';
				charadd();
				break;
			case ALLEGRO_KEY_1:
				ch = '1';
				charadd();
				break;
			case ALLEGRO_KEY_2:
				ch = '2';
				charadd()
				break;
			case ALLEGRO_KEY_3:
				ch = '3';
				charadd()
				break;
			case ALLEGRO_KEY_4:
				ch = '4';
				charadd()
				break;
			case ALLEGRO_KEY_5:
				ch = '5';
				charadd()
				break;
			case ALLEGRO_KEY_6:
				ch = '6';
				charadd()
				break;
			case ALLEGRO_KEY_7:
				ch = '7';
				charadd()
				break;
			case ALLEGRO_KEY_8:
				ch = '8';
				charadd()
				break;
			case ALLEGRO_KEY_9:
				ch = '9';
				charadd()
			}
		}
		else if (event.type == ALLEGRO_EVENT_KEY_UP) pressed_key = false;

		if (redraw && al_is_event_queue_empty(queue)) {
			redraw = false;

			al_clear_to_color(al_map_rgb(0, 0, 0));
			char arrow[3] = { ' ', ' ', ' ' };
			arrow[selected_line] = '>';

			al_draw_text(font, al_map_rgb(255, 255, 255), 2, 2, 0, "Enter friction parameter, angle and speed (up/down arrows), press enter to throw (only 1 at a time), press esc to quit");
			al_draw_text(font, al_map_rgb(255, 255, 255), 2, 14, 0, "The ball's diameter - 1 m; weight - 1 kg; to effectively change the mass divide friction by new mass");
			char text[44] = "  Friction (0-1): ";
			text[0] = arrow[0];
			strcat(text, inputs[0]);
			al_draw_text(font, al_map_rgb(255, 255, 255), 2, 26, 0, text);
			strcpy(text, "  Angle (0-90 degrees): ");
			text[0] = arrow[1];
			strcat(text, inputs[1]);
			al_draw_text(font, al_map_rgb(255, 255, 255), 2, 38, 0, text);
			strcpy(text, "  Speed: ");
			text[0] = arrow[2];
			strcat(text, inputs[2]);
			al_draw_text(font, al_map_rgb(255, 255, 255), 2, 50, 0, text);

			if (launching) {
				ALLEGRO_LOCKED_REGION* bmp = al_lock_bitmap(display_bitmap, al_get_bitmap_format(display_bitmap), ALLEGRO_LOCK_READWRITE);
				throw((uint32_t*)bmp->data, k, v, pos, time);
				al_unlock_bitmap(display_bitmap);

				if (pos[0] < 0 || pos[1] > X_LIM) launching = false;
			}			
			al_flip_display();	
		}
	}
}